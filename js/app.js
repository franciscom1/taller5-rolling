import { getProductos } from "../helppers/fetch.js";

// getProductos().then((resp) => console.log(resp));

class UI {
  static crearMenu(categorias) {
    const menu = document.querySelector("#lista_categorias");
    categorias.forEach((categoria) => {
      const option = document.createElement("option");
      option.value = `${categoria}`;
      option.innerHTML = `${categoria}`;
      menu.appendChild(option);
    });
  }
  static crearCards(productos) {
    const mensaje = document.querySelector("#mensaje");
    const containerCards = document.querySelector("#container_cards");
    containerCards.classList = "justify-content-between d-flex flex-wrap";
    mensaje.innerHTML = "";
    containerCards.innerHTML = "";
    productos.forEach((producto) => {
      const {
        image,
        title,
        price,
        rating: { rate },
      } = producto;
      const card = document.createElement("div");
      card.classList =
        "d-flex col-5 my-1 p-2 justify-content-between border rounded";
      card.innerHTML = `<div>
      <img src="${image}" class='img-fluid max-width: 50%'/>
      </div>
      <div>
      <h4>${title}</h4>
      <p class='text-left'>${price}</p>
      <div class ='d-flex justify-content-end'>
        <button type="button" class="btn btn-warning">
        Calificacion <span class="badge badge-light">${rate}</span>
        </button>
      </div>
      </div>`;
      containerCards.appendChild(card);
    });
  }
}

class Store {
  static getProductos(option = "todo") {
    getProductos().then((resp) => {
      const categorias = resp.map((producto) => producto.category);
      UI.crearMenu([...new Set(categorias)]);
      Store.filtrarCategorias(resp, option);
    });
  }

  static filtrarCategorias(productos, option) {
    if (option === "todo") {
      UI.crearCards(productos);
    } else {
      const categorias = productos.filter(
        (producto) => producto.category === option
      );
      return UI.crearCards(categorias);
    }
  }
}

Store.getProductos();

document
  .querySelector("#lista_categorias")
  .addEventListener("change", (e) => Store.getProductos(e.target.value));
